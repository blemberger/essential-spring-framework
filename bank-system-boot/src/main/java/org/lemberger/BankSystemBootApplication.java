package org.lemberger;

import org.h2.tools.Server;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.SQLException;

@SpringBootApplication
public class BankSystemBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankSystemBootApplication.class, args);
	}

	// Can access the H2 Admin page at http://localhost:8082
    // Use this JDBC URL: jdbc:h2:mem:bsl-test-db
	@Bean(value = "h2WebServer", destroyMethod = "")
	public Server h2WebServer() throws SQLException {
		return Server.createWebServer("-webAllowOthers", "-webPort", "8082").start();
	}
}
