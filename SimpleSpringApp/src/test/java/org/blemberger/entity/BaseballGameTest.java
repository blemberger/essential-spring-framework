package org.blemberger.entity;

import org.blemberger.GameConfig;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.sql.DataSource;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = GameConfig.class)
public class BaseballGameTest {

    @Autowired
    private Game baseballGame;

    @Autowired
    private ApplicationContext context;

    @Test
    public void testPlayGame() {
        String homeTeam = baseballGame.getHomeTeam().getName();
        String visitingTeam = baseballGame.getVistingTeam().getName();

        String result = baseballGame.playGame();

        assertThat(result, CoreMatchers.anyOf(
                containsString(homeTeam),
                containsString(visitingTeam),
                containsString(Game.RAINED_OUT_RESULT)));
    }

    @Test
    public void testSetGetterHomeTeam() {
        Team royals = context.getBean("royals", Royals.class);
        baseballGame.setHomeTeam(royals);

        assertThat(baseballGame.getHomeTeam(), is(royals));
    }

    @Test
    public void testDataSourceSet() {
        DataSource ds = context.getBean("dataSource", DataSource.class);
        assertThat(baseballGame.getDataSource(), is(ds));
    }
}