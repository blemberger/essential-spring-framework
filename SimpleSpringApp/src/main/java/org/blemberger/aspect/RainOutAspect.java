package org.blemberger.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.blemberger.entity.Game;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class RainOutAspect {

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Around("execution(String playGame())")
    public String checkForRain(ProceedingJoinPoint pjp) throws Throwable {
        String result = null;
        if (Math.random() < 0.3) {
            logger.info(pjp.getTarget().toString() + " game rained out");
            result = Game.RAINED_OUT_RESULT;
        }
        else {
            result = (String)pjp.proceed(); // safe as long as pointcut expression only applies to String return type methods
            logger.info("Winner of " + pjp.getTarget().toString() + ": " + result);
        }
        return result;
    }
}
