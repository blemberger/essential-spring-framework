package org.blemberger.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.util.logging.Logger;

@Aspect
@Component
public class LoggingAspect {

    private final Logger logger = Logger.getLogger(getClass().getName());

    @Before("execution(void org.blemberger..*.set*(*))")
    public void logSetters(JoinPoint jp) {
        String methodName = jp.getSignature().getName();
        String arg = jp.getArgs()[0].toString();
        String targetName = jp.getTarget().toString();
        logger.info("Called " + methodName + " with " + arg + " on " + targetName);
    }
}
