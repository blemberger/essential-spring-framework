package org.blemberger;

import org.blemberger.entity.BaseballGame;
import org.blemberger.entity.Game;
import org.blemberger.entity.Team;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.sql.DataSource;
import java.text.NumberFormat;

public class RunDemo {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(GameConfig.class);
//        ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");

        Game cubsVSoxBean = context.getBean("game", Game.class);
        Team royals = context.getBean("royals", Team.class);
        Team cubs = context.getBean("cubs", Team.class);
        Team whiteSox = context.getBean("whiteSox", Team.class);

        cubsVSoxBean.playGame();

        Game royalsVSoxBean = context.getBean("game", Game.class);
        royalsVSoxBean.setVisitingTeam(royals);
        royalsVSoxBean.playGame();

        // Total hack - showing that aspects & pointcuts only work on spring managed beans
        BaseballGame royalsVSoxNonBean = new BaseballGame(whiteSox, royals); // Not a spring bean
        System.out.println("Winner: " + royalsVSoxNonBean.playGame());

        NumberFormat numberFormat = context.getBean("nf", NumberFormat.class);
        double f = 128384884.333288283;
        System.out.println(numberFormat.format(f));

//        System.out.println("There are " + context.getBeanDefinitionCount() + " beans defined.");
//        for (String name : context.getBeanDefinitionNames()) {
//            System.out.println(name);
//        }
    }
}
