package org.blemberger.entity;

import org.springframework.stereotype.Component;

@Component
public class WhiteSox extends AbstractTeam {
    public String getName() {
        return "Chicago White Sox";
    }
}
