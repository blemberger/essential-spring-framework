package org.blemberger.entity;

import javax.sql.DataSource;

public interface Game {
    String RAINED_OUT_RESULT = "Rained out";

    void setHomeTeam(Team t);
    Team getHomeTeam();
    void setVisitingTeam(Team t);
    Team getVistingTeam();

    String playGame();

    DataSource getDataSource();
}
