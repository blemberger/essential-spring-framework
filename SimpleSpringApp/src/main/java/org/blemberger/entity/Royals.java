package org.blemberger.entity;

import org.springframework.stereotype.Component;

@Component
public class Royals extends AbstractTeam {
    public String getName() {
        return "Kansas City Royals";
    }
}
