package org.blemberger.entity;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import java.util.logging.Logger;

public class BaseballGame implements Game {

    private final Logger logger = Logger.getLogger(getClass().getName());

    private Team homeTeam;

    private Team visitingTeam;

    private DataSource dataSource;

    public BaseballGame(Team home, Team visiting) {
        homeTeam = home;
        visitingTeam = visiting;
    }

    @PostConstruct
    public void startGame() {
        logger.info("Playing national anthem");
    }

    @PreDestroy
    public void endGame() {
        logger.info("Sending highlights to MLB");
    }

    public void setHomeTeam(Team t) {
        homeTeam = t;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public void setVisitingTeam(Team t) {
        visitingTeam = t;
    }

    public Team getVistingTeam() {
        return visitingTeam;
    }

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public DataSource getDataSource() {
        return this.dataSource;
    }

    public String playGame() {
        return Math.random() < 0.5 ? homeTeam.getName() : visitingTeam.getName();
    }

    @Override
    public String toString() {
        return String.format("%s at %s", visitingTeam.getName(), homeTeam.getName());
    }
}
