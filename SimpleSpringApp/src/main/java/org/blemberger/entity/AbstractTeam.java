package org.blemberger.entity;

public abstract class AbstractTeam implements Team {

    @Override
    public String toString() {
        return getName();
    }
}
