package org.blemberger.entity;

import org.springframework.stereotype.Component;

@Component
public class Cubs extends AbstractTeam {
    public String getName() {
        return "Chicago Cubs";
    }
}