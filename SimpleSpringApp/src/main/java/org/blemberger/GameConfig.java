package org.blemberger;

import org.blemberger.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.*;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.text.NumberFormat;

@Configuration
@ComponentScan("org.blemberger")
@EnableAspectJAutoProxy
public class GameConfig {

    // Autowired annotation requires @Qualifier, since there is more than one bean in this context of type Team.
    @Autowired @Qualifier("whiteSox")
    private Team home;

    // Can also use JSR @Resource annotation, giving the name of the bean
    @Resource(name = "cubs")
    private Team away;

    @Bean
    @Scope("prototype")
    // Pre-Destroy annotation and destroy-method attribute to @Bean only work for scope = singleton
    public Game game() {
        return new BaseballGame(home, away);
    }

    @Bean
    public NumberFormat nf() {
        return NumberFormat.getInstance();
    }
}
