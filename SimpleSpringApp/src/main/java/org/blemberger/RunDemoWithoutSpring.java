package org.blemberger;

import org.blemberger.entity.*;

public class RunDemoWithoutSpring {

    public static void main(String[] args) {
        Team whiteSox = new WhiteSox();
        Team cubs = new Cubs();
        Game game = new BaseballGame(whiteSox, cubs);
        System.out.println(game.playGame());
    }
}
