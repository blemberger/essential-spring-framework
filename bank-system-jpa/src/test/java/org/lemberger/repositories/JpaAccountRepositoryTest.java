package org.lemberger.repositories;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.lemberger.config.AppConfig;
import org.lemberger.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@Transactional
public class JpaAccountRepositoryTest {

    @Autowired
    private AccountRepository repository;

    @Test
    public void testGetAccounts() {
        List<Account> accounts = repository.getAccounts();
        assertThat(accounts.size(), is(3));
    }

    @Test
    public void testGetAccount() {
        Account account = repository.getAccount(1L);
        assertThat(account.getId(), is(1L));
        assertThat(account.getBalance(), is(closeTo(new BigDecimal("100.0"), new BigDecimal("0.01"))));
    }

    @Test
    public void testGetNumberOfAccounts() {
        assertThat(repository.getNumberOfAccounts(), is(3));
    }

    @Test
    public void testCreateAccount() {
        Long id = repository.createAccount(new BigDecimal("250.0"));
        assertThat(id, is(notNullValue()));

        Account account = repository.getAccount(id);
        assertThat(account.getId(), is(id));
        assertThat(account.getBalance(), is(closeTo(new BigDecimal("250.0"), new BigDecimal("0.01"))));
    }

    @Test
    public void testUpdateAccount() {
        Account account = repository.getAccount(1L);
        BigDecimal current = account.getBalance();
        BigDecimal amount = new BigDecimal("50.0");
        account.setBalance(current.add(amount));

        Account again = repository.getAccount(1L);
        assertThat(again.getBalance(), is(closeTo(current.add(amount), new BigDecimal("0.01"))));
    }

    @Test
    public void testDeleteAccount() {
        for (Account account : repository.getAccounts()) {
            repository.deleteAccount(account.getId());
        }
        assertThat(repository.getNumberOfAccounts(), is(0));
    }
}