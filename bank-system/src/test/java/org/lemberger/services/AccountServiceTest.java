package org.lemberger.services;

import org.hamcrest.number.BigDecimalCloseTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.lemberger.config.AppConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.number.BigDecimalCloseTo.closeTo;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = AppConfig.class)
@Transactional
@ActiveProfiles("test")
public class AccountServiceTest {

    @Autowired
    private AccountService service;

    /**
     * This test was not part of the Essential Spring Framework Course
     * @author blemberger
     */
    @Test
    public void testGetBalance() {
        assertThat(service.getBalance(1L), is(closeTo(new BigDecimal("100.0"), new BigDecimal(("0.01")))));
    }

    @Test
    public void testDeposit() {
        BigDecimal start = service.getBalance(1L);
        BigDecimal amount = new BigDecimal("50.0");
        service.deposit(1L, amount);
        BigDecimal finish = start.add(amount);

        assertThat(service.getBalance(1L), is(closeTo(finish, new BigDecimal("0.01"))));
    }

    @Test
    public void testWithdraw() {
        BigDecimal start = service.getBalance(1L);
        BigDecimal amount = new BigDecimal("50.0");
        service.withdraw(1L, amount);
        BigDecimal finish = start.subtract(amount);

        assertThat(service.getBalance(1L), is(closeTo(finish, new BigDecimal("0.01"))));
    }

    @Test
    public void testTransfer() {
        BigDecimal startAcct1 = service.getBalance(1L);
        BigDecimal startAcct2 = service.getBalance(2L);
        BigDecimal amount = new BigDecimal("50.0");
        service.transfer(1L, 2L, amount);

        BigDecimal finishAcct1 = startAcct1.subtract(amount);
        BigDecimal finishAcct2 = startAcct2.add(amount);

        assertThat(service.getBalance(1L), is(closeTo(finishAcct1, new BigDecimal("0.01"))));
        assertThat(service.getBalance(2L), is(closeTo(finishAcct2, new BigDecimal("0.01"))));
    }
}