package org.lemberger.config;

import org.apache.commons.dbcp2.BasicDataSource;
import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@ComponentScan("org.lemberger")
@PropertySource("classpath:prod.properties")
@EnableTransactionManagement
public class AppConfig {

    @Autowired
    private Environment env;

    @Bean(name = "dataSource", destroyMethod = "shutdown")
    @Profile("test")
    public DataSource dataSourceForTest() {
        return new EmbeddedDatabaseBuilder()
                .setName("accountdb")
                .setType(H2)
                .setScriptEncoding("UTF-8")
                .ignoreFailedDrops(true)
                .addScripts("schema.sql", "data.sql")
                .build();
    }

    // Can access the H2 Admin page at http://localhost:8082
    @Bean(value = "h2WebServer", destroyMethod = "")
    @Profile("test")
    public Server h2WebServer() throws SQLException{
        return Server.createWebServer("-webAllowOthers", "-webPort", "8082").start();
    }

    @Profile("test")
    @Bean("transactionManager")
    public PlatformTransactionManager transactionManagerForTest() {
        return new DataSourceTransactionManager((dataSourceForTest()));
    }

    @Bean(name = "dataSource")
    @Profile("prod")
    public DataSource dataSourceforProd() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDriverClassName(env.getProperty("db.driver"));
        dataSource.setUrl(env.getProperty("db.url"));
        dataSource.setUsername(env.getProperty("db.user"));
        dataSource.setPassword(env.getProperty("db.pass"));
        return dataSource;
    }

    @Profile("prod")
    @Bean("transactionManager")
    public PlatformTransactionManager transactionManagerForProd() {
        return new DataSourceTransactionManager((dataSourceforProd()));
    }

}
