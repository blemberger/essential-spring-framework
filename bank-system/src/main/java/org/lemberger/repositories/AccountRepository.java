package org.lemberger.repositories;

import org.lemberger.entities.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountRepository {

    List<Account> getAccounts();

    Account getAccount(Long id);

    int getNumberOfAccounts();

    long createAccount(BigDecimal initialBalance);

    int deleteAccount(Long id);

    void updateAccount(Account account);
}
