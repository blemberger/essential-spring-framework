package org.lemberger.repositories;

import org.lemberger.entities.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Repository
@Profile("test")
public class JdbcAccountRepository implements AccountRepository{

    private JdbcTemplate template;
    private static long nextInt = 4;

    @Autowired
    public JdbcAccountRepository(DataSource dataSource) {
        this.template = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Account> getAccounts() {
        String sqlText = "select * from account";
        return template.query(sqlText, new AccountMapper());
    }

    @Override
    public Account getAccount(Long id) {
        String sqlText = "select * from account where id=?";
        return template.queryForObject(sqlText, new AccountMapper(), id);
    }

    @Override
    public int getNumberOfAccounts() {
        String sqlText = "select count(*) from account";
        return template.queryForObject(sqlText, Integer.class);
    }

    @Override
    public long createAccount(BigDecimal initialBalance) {
        String sqlText = "insert into account(id, balance) values(?, ?)";
        long id = nextInt++;
        template.update(sqlText, id, initialBalance);
        return id;
    }

    @Override
    public int deleteAccount(Long id) {
        String sqlText = "delete from account where id = ?";
        return template.update(sqlText, id);
    }

    @Override
    public void updateAccount(Account account) {
        String sqlText = "update account set balance = ? where id = ?";
        template.update(sqlText, account.getBalance(), account.getId());
    }

    private static class AccountMapper implements RowMapper<Account> {

        public Account mapRow(ResultSet resultSet, int i) throws SQLException {
            return new Account(resultSet.getLong("id"), resultSet.getBigDecimal("balance"));
        }
    }
}
